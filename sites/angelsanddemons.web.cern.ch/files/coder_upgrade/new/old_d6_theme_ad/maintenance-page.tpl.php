﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php print $head_title ?></title>
    
               
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    
    

    

  </head>
  
<body<?php print phptemplate_body_class($left, $right, $language); ?>>








<?php include 'includes/global_banner.inc' ?>




 

    
    <?php if ($breadcrumb): print $breadcrumb; endif; ?>

   		
      
      

      <?php if ($left): ?>
     <!-- LEFT SIDEBAR: -->
        <div id="sidebar-left">
			
			  <?php print $left ?>
			
        </div>
      <?php endif; ?><!-- /left sidebar -->
      
      
      
      
      
      
	<!-- MAIN CONTENT: -->
      <div id="content">
         
         
         
			  
			 
			 
			 			 
			  <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
			  <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
			  <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
			  <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
			  <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
			  <?php if ($show_messages && $messages): print $messages; endif; ?>
			  <?php print $help; ?>
			  			  
			  
			  <?php print $content ?>
			  
			 
			  
			  
			  <?php print $feed_icons ?>
         		
         		
         	
         
         
      </div><!-- /content -->
      







      <?php if ($right): ?>
      	<!-- RIGHT SIDEBAR: -->
      	<div id="sidebar-right">
        	
			  
			  <?php print $right ?>
        	
        </div>
      <?php endif; ?><!-- /right sidebar -->
      
      
      
      
      
<?php
		
		if ($language) {
		
			if ($language->native == "English") {
				include 'includes/global_footer.inc';
			}
			
			elseif ($language->native == "Français") {
				include 'includes/global_footer_FR.inc';
			}
			
		}
		?>


 
 
</body>
</html>
