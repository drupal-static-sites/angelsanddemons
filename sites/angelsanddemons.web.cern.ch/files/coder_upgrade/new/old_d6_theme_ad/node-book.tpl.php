﻿<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> <?php print $type ?>">

<?php print $picture ?>




<?php if (!$page): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>



  <div class="content">
	
	<?php $img_main = $node->field_img_main[0]['value']; ?>
	
	 
	 <?php if ($img_main): ?>
	 	
	 	<?php $img_main_caption = $node->field_img_main_caption[0]['value']; ?>
	 	<?php $img_main_alt = $node->field_img_main_alt[0]['value']; ?>
	 	
	 	<div id="main_img">
	 		<img src="<?php print $img_main ?>" alt="<?php print $img_main_alt ?>" />
	 		<?php if ($img_main_caption): ?>
	 			<p class="caption"><?php print $img_main_caption ?></p>
	 		<?php endif; ?>
	 		
	 	</div><!--/main_img-->
	
	<?php endif; ?>

		
		
	
    <?php print $content ?>
  </div>




 
</div>


