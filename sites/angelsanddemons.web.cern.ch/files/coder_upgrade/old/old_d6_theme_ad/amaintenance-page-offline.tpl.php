<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php print $head_title ?></title>
    
               
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    
    

    

  </head>
  
<body>








<?php include 'includes/global_banner.inc' ?>




 

    
    <?php if ($breadcrumb): print $breadcrumb; endif; ?>

   		
      
      

      <?php if ($left): ?>
     <!-- LEFT SIDEBAR: -->
        <div id="sidebar-left">
			
			  <?php print $left ?>
			
        </div>
      <?php endif; ?><!-- /left sidebar -->
      
      
      
      
      
      
	<!-- MAIN CONTENT: -->
      <div id="content">
         
         

			  
			  <?php print $content ?>
			  
			 
			 		  
			 
         		
         		
         	
         
         
      </div><!-- /content -->
      







      <?php if ($right): ?>
      	<!-- RIGHT SIDEBAR: -->
      	<div id="sidebar-right">
        	
			  
			  <?php print $right ?>
        	
        </div>
      <?php endif; ?><!-- /right sidebar -->
      
      
      
      
      


<?php include 'includes/global_footer.inc' ?>

 
 
</body>
</html>
