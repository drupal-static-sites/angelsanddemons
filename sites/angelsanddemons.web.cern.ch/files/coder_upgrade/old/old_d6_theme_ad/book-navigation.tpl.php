
<div class="page-links clear-block">

	
	<?php if ($next_url): ?>
		<p class="right"><a class="page-next" title="Go to next page" href="<?php print $next_url ?>"><?php print $next_title ?> &raquo;</a></p>
	<?php endif; ?>

	
</div>