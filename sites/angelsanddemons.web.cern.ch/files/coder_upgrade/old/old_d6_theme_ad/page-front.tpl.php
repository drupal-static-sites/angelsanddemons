﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php print $head_title ?></title>
    
               
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    
    

    

  </head>
  
<body<?php print phptemplate_body_class($left, $right, $language); ?> id="front_page">

<?php include 'includes/global_banner.inc' ?>



   
    <?php if ($breadcrumb): print $breadcrumb; endif; ?>

   		
      

      
      
      
      
	<!-- MAIN CONTENT: -->
      <div id="content">
         
         	 <?php print $front_synopsis ?>      
         
      </div><!-- /content -->
      







      
      	<!-- RIGHT SIDEBAR: -->
      	<div id="sidebar-right">
        	  
			  <?php print $right ?>
        	
        </div>
      
      
  
    
 

		<?php
		
		if ($language) {
		
			if ($language->native == "English") {
				include 'includes/global_footer.inc';
			}
			
			elseif ($language->native == "Français") {
				include 'includes/global_footer_FR.inc';
			}
			
		}
		?>


 

  </body>
</html>
