﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php print $head_title ?></title>
    
               
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    
    

    

  </head>
  
<body<?php print phptemplate_body_class($left, $right, $language); ?>>


<h1>Custom login!</h1>


<div id="page_container">

<div id="page_wrapper">

<?php include 'includes/global_banner.inc' ?>




<!-- Layout -->


 


 
 
 

 
    
    <div id="page">
    
    <?php if ($breadcrumb): print $breadcrumb; endif; ?>

   		
      
      

      <?php if ($left): ?>
     <!-- LEFT SIDEBAR: -->
        <div id="sidebar-left">
			
			  <?php print $left ?>
			
        </div>
      <?php endif; ?><!-- /left sidebar -->
      
      
      
      
      
      
	<!-- MAIN CONTENT: -->
      <div id="content">
         
         
         
			  
			 
			  
			  <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
			  <?php if ($title): print '<h2>'. $title .'</h2>'; endif; ?>
			
			  <?php print $help; ?>
			  
			  
			  <?php print $content ?>
			  
			  
			 
			  
			  <?php print $feed_icons ?>
         		
         		
         	
         
         
      </div><!-- /content -->
      







      <?php if ($right): ?>
      	<!-- RIGHT SIDEBAR: -->
      	<div id="sidebar-right">
        	
			  
			  <?php print $right ?>
        	
        </div>
      <?php endif; ?><!-- /right sidebar -->
      
      
      
      
      
<br clear="all" />
    </div> <!-- /page -->
    
    
     <?php if ($site_footer): ?>
        <div id="side_footer">
        	
			  <?php print $site_footer ?>
        	
        </div>
      <?php endif; ?><!-- /site_footer -->

<?php include 'includes/global_footer.inc' ?>

 
<!-- /layout -->

 
  
</div><!-- /page_wrapper -->
</div><!-- /page_container -->
  </body>
</html>
