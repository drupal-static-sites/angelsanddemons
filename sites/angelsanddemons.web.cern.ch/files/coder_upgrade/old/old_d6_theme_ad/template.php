<?php


/* Custom login page */

function _phptemplate_variables($hook, $vars = array()) {
  switch ($hook) {
    case 'page':
      global $user;
      if (arg(0) == 'user'){
        if ($user->uid == 0) {
          $vars['template_file'] = 'page-login';
        }
        elseif (arg(1) == 'login' || arg(1) == 'register' || arg(1) == 'password' || arg(1) == 'user' ) {
          $vars['template_file'] = 'page-login';
        }
      }
      break;
  }

  return $vars;
}




/**
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */
function phptemplate_body_class($left, $right, $language) {
  if ($left != '' && $right != '') {
    $class = 'sidebars';
  }
  else {
    if ($left != '') {
      $class = 'sidebar-left';
    }
    if ($right != '') {
      $class = 'sidebar-right';
    }
  }

    print ' class="'. $class . ' lang-'. $language->language .'"';

}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $node) {
  if (!$content || $node->type == 'forum') {
    return '<div id="comments">'. $content .'</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}


/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  return menu_primary_local_tasks();
}

function phptemplate_comment_submitted($comment) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
    ));
}

function phptemplate_node_submitted($node) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}



function cern_preprocess_page(&$variables) {
  if (isset($variables['node'])) {
    // suggest template based on content type.
    $variables['template_files'][] =  'page-'. str_replace('_', '-', $variables['node']->type);
  }
}


