<div class="video_button">

	<?php $img_thumb = $fields['field_img_thumb_value']->content; ?>
	<?php $node_url = url("node/" . $row->nid); ?>
	
	<?php if ($img_thumb): ?>
	
		<?php $img_alt = $fields['field_img_main_alt_value']->content; ?>
	
		<div class="thumb_wrapper">	
			<a href="<?php print $node_url; ?>"><img src="<?php print $img_thumb; ?>"<?php if ($img_alt): print ' alt="' . $img_alt . '"'; endif; ?>	/></a>
		</div>		
		
	<?php endif; ?>
	
	
	<h3><?php print $fields['title']->content; ?></h3>
	
</div>